# Practico 1
## Clustering de palabras

### Objetivo

Encontrar grupos de palabras que puedan ser usados como clases de equivalencia, al estilo de los Brown Clusters.

### Procedimiento

* Obtener un corpus de la lengua española (por ejemplo, alguno del SBWCE de Cristian Cardellino, o, a falta de mejores opciones, se pueden usar los que hay en mi directorio de corpus)
* Preprocesarlo para normalizar las palabras, con NLTK o Freeling
    - se pueden realizar análisis morfosintáctico o sintáctico, de forma de enriquecer la información asociada a palabras, por ejemplo, con triplas de dependencias.
* Vectorizar las palabras (quizás usando el vectorizador de scikitlearn).
    - al momento de vectorizar las palabras se pueden usar diferentes criterios para reducción de dimensionalidad (por ejemplo, umbral de frecuencia), y también LSA o word embeddings neuronales.
* Obtener clusters de palabras.
* Analizar los clusters, iterar a 3.

